import datetime
import logging
import os
import bz2

def get_ouput_path(video_doc: dict, filename: str, output_dir: str) -> str:
    iso_time = video_doc.get("recorded_at", None)

    video_time = datetime.datetime.strptime(iso_time, "%Y-%m-%dT%H:%M:%S.%fZ")
    # Format: user/yyyy-mm-dd/session/id/<files>
    user = video_doc.get("user", "")
    date = video_time.strftime("%Y-%m-%d")
    session = video_doc['session_tag']
    video_id = video_doc['_id']

    os.path.sep

    return os.path.join(output_dir, user, date, session, video_id, filename)


def save_bz2_file(data: str, output_file_name: str) -> str:
    """ Deflate with bz2 (1/7 of original) and save """
    try:
        os.makedirs(os.path.dirname(output_file_name), exist_ok=True)
        if data is None:
            return ""

        with bz2.BZ2File(output_file_name + ".bz2", 'w') as handle:
            handle.write(data.encode())
        return output_file_name
    except IOError:
        logging.error("Failed to save file %s", output_file_name)
        return ""


def save_file(data: str, output_file_name: str) -> str:
    """ Simple file save wrapper (to be extended with compression) """
    try:
        os.makedirs(os.path.dirname(output_file_name), exist_ok=True)

        with open(output_file_name, 'w') as out_file:
            out_file.write(data)
        return output_file_name
    except IOError:
        logging.error("Failed to save file %s", output_file_name)
        return ""
