""" Example downloading raw data from share """
import os
import logging
from typing import Optional, Dict
import unittest

from roadai.api import Api
from roadai.util import io

FR_B = "%(asctime)s,%(msecs)d %(levelname)-8s"
FR_E = ' [%(filename)s:%(lineno)d] %(message)s'
DATE_FRM = '%d-%m-%Y:%H:%M:%S'
logging.basicConfig(format=FR_B + FR_E, datefmt=DATE_FRM, level=logging.DEBUG)


def get_share(api, share_name) -> Optional[Dict]:
    """ Get certain share """
    share = None
    shares = api.shares()
    if not share_name and shares:
        logging.warning("No specific share wanted, selecting first")
        share = shares[0]
    elif shares:
        share = next((i for i in shares if i['name'] == share_name), None)

    if share is None:
        logging.error("No share %s defined", share_name)
        return None
    return share


def download_files(api, videos, output_dir, download_serial=False, max_videos=5) -> bool:
    logging.info("Loaded info for %d videos", len(videos))

    download_filenames = ["location.gpx", "accelerometer.csv", "gyroscope.csv"]
    if download_serial:
        download_filenames.append("serial.csv")

    logging.info("Downloading raw data for %d/%d videos", max_videos,
                 len(videos))
    for idx, video in enumerate(videos):
        if idx == max_videos:
            break
        for filename in download_filenames:
            output_path = io.get_ouput_path(video, filename, output_dir)
            if not os.path.exists(output_path):  # Do not redownload
                content = api.download_file(video, filename)
                if not content:
                    continue
                io.save_bz2_file(content, output_path)
    return True


def download_data_to_local_from_share(username: str,
                                      password: str,
                                      output_dir: str,
                                      max_videos=5,
                                      share_name=None,
                                      download_serial=False) -> Optional[Dict]:
    """ Download a share to local storage using a session """
    api = Api()
    auth_data = api.login(username, password)
    if not auth_data:
        logging.error("No auth! %s, %s", username, password)
        return None

    share = get_share(api, share_name)
    if not share:
        return None
    client_id = share.get('client_id', None)
    if client_id is None:
        logging.error("Share %s did not have client_id", str(share))
        return None

    parameter_type = None
    if download_serial is True:
        parameter_type = "SURFACE_TEMPERATURE"

    videos = api.mobileObservations(
        limit=100, share_id=share["id"], parameter_type=parameter_type)
    if not videos:
        logging.error("Did not get videos")
        return None

    download_files(api, videos, download_serial, max_videos, output_dir)
    api.logout()
    return share


class TestDownload(unittest.TestCase):
    """ Testing downloading data to local """

    def test_download(self):
        """ Test that downloading files work """
        username: str = os.getenv('API_USERNAME', 'roadai-api-testuser')
        password: str = os.getenv('API_PASSWORD',
                                  'ydWcqd69KfRXnLdPkpzcjLKeSSXSvBXD')
        output_dir = "/output"
        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)

        share = download_data_to_local_from_share(username, password,
                                                  output_dir)
        self.assertTrue(share is not None)


if __name__ == "__main__":
    unittest.main()
