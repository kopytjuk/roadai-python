""" Downloader module for data """
import logging
import requests
from requests.exceptions import RequestException
from typing import Dict, Optional


class Downloader:
    # URL = os.getenv('STATIC_URL', 'https://static.vionice.io/videos/')

    def connect(self, auth_data: Dict, silent=False):
        """ Create a session and set static asset server with session"""
        if not silent and self.session is not None:
            raise TypeError("self.session is defined")

        self.session: requests.Session = requests.Session()
        self.set_headers(auth_data)

    def set_headers(self, auth_data: Dict):
        headers = {
            'X-Auth-Token': auth_data['authToken'],
            'X-User-Id': auth_data['userId'],
            #'X-Client-Id': client_id
        }
        self.session.headers.update(headers)

    def download_file(self, url: str) -> Optional[str]:
        """ Downloads file from static asset server with session"""
        try:
            resp = self.session.get(url, timeout=30)
        except RequestException as err:
            logging.error("Request error: %s", str(err))
            return None

        if resp.status_code == 200:
            return resp.text
        if resp.status_code == 404:
            logging.error("File not found: (status %d)", resp.status_code)
            return None

        logging.error("Unkown: %d %s\n %s \n", resp.status_code, resp.text, url)
        return None
