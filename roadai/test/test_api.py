import unittest
from typing import List, Tuple
from geojson import Polygon
from datetime import datetime

from roadai.api import Api
from roadai.download import Downloader
from roadai.api.parse_filters import polygonToCsv, datetimeToQueryParam


class ApiTest(unittest.TestCase):
    def test_api_Api(self):
        api = Api()

        self.assertIsInstance(api, Api)
        self.assertIsInstance(api.downloader, Downloader)

    def test_api_polygonToCsv(self):
        coordinates: List[Tuple[float, float]] = [(28.1884074, 61.0545937),
                                                  (28.1889868, 61.0534565),
                                                  (28.1928921, 61.0539498),
                                                  (28.1928921, 61.0545937),
                                                  (28.1884074, 61.0545937)]

        p1 = Polygon([coordinates])
        csv: str = "28.1884074,61.0545937,28.1889868,61.0534565,28.1928921,61.0539498,28.1928921,61.0545937,28.1884074,61.0545937"

        self.assertEqual(polygonToCsv(p1), csv)

    def test_api_datetimeToQueryParams(self):

        time: datetime = datetime(2018, 10, 16, 14, 25, 51, 640544)
        output: str = "2018-10-16T14:25:51.640544"

        self.assertEqual(datetimeToQueryParam(time), output)


if __name__ == "__main__":
    unittest.main()
