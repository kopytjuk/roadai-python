""" Main API """
import copy
import datetime
import os
import logging
import json
from typing import Dict, Any, Union, List

from geojson import Polygon
import requests
from requests.exceptions import RequestException

from ..download import Downloader
from .util import depaginate, api_post, api_patch, api_get, _api_generic
from .parse_filters import polygonToCsv, datetimeToQueryParam


def parseMobileObservationsQuery(share_id: str = None,
                                 date_from_created: datetime.datetime = None,
                                 date_to_created: datetime.datetime = None,
                                 date_from_recorded: datetime.datetime = None,
                                 date_to_recorded: datetime.datetime = None,
                                 geometry_polygon: Polygon = None,
                                 parameter_type: str = None,
                                 client_id: str = None,
                                 limit: int = 100,
                                 offset: int = None,
                                 fields: str = None):

    params: Dict[str, Union[str, int]] = {}

    if share_id is not None:
        params.update({"share_id": share_id})

    if date_from_created is not None:
        params.update({
            "date_from_created":
            datetimeToQueryParam(date_from_created)
        })

    if date_to_created is not None:
        params.update({
            "date_to_created": datetimeToQueryParam(date_to_created)
        })

    if date_from_recorded is not None:
        params.update({
            "date_from_recorded":
            datetimeToQueryParam(date_from_recorded)
        })

    if date_to_recorded is not None:
        params.update({
            "date_to_recorded":
            datetimeToQueryParam(date_to_recorded)
        })

    if geometry_polygon is not None:
        params.update({"geometry_polygon": polygonToCsv(geometry_polygon)})

    if parameter_type is not None:
        params.update({"parameter_type": parameter_type})

    if client_id is not None:
        params.update({"client_id": client_id})

    if limit is not None:
        params.update({"limit": limit})

    if offset is not None:
        params.update({"offset": offset})

    if fields is not None:
        params.update({"fields": fields})

    return params


class Api:
    URL = os.getenv('API_URL', 'https://map.vionice.io/api/v1/')
    NETWORK_ANALYZER_URL = 'https://rfn-painter-api.vionice.io/'

    def __init__(self):
        self.auth = None
        self.downloader = Downloader()

    def internal_login(self, user_id: str) -> Any:
        """
        Login to Vionice's Services inside the cluster

        Args:
            :param user_id: user id

        Returns:
            The authentication default.
        """

        self.auth = {'internal': True, 'userId': user_id}

        return self.auth

    def set_auth(self, user_id, auth_token):
        self.auth = {
            'authToken': auth_token,
            'userId': user_id,
            'token_time': datetime.datetime.now()
        }

        return self.auth

    def login(self, username: str, password: str) -> Any:
        """ Login to Vionice's Services. Return auth_data. """

        logging.info("Authenticating against API")
        post_data = {'username': username, 'password': password}
        try:
            resp = requests.post(
                Api.URL + "login/", data=post_data, timeout=10)
        except RequestException as err:
            logging.error("IOError: %s", str(err))
            self.auth = None

        if resp.status_code == 200:
            logging.info(resp.text)
            authentication = resp.json()['data']
            authentication['token_time'] = datetime.datetime.now()
            authentication['username'] = username
            authentication['password'] = password
            self.auth = authentication

            self.downloader.connect(authentication, silent=True)

            return self.auth

        logging.error("Failed to login: %d %s", resp.status_code, resp.text)
        self.auth = None

        return False

    def logout(self) -> bool:
        """ Logout from Vionice's Services. """

        if self.auth.get('internal'):
            self.auth = None

            return True

        logging.info("Logging out!")
        headers = {
            'X-Auth-Token': self.auth['authToken'],
            'X-User-Id': self.auth['userId'],
        }
        try:
            resp = requests.get(
                Api.URL + "logout/", headers=headers, timeout=30)
        except RequestException as err:
            logging.error("IOError: %s", str(err))

            return False

        if resp.status_code == 200:
            logging.info(resp.text)
            message = resp.json()['data']
            logging.info(message['message'])
            self.auth = None

            return True

        logging.error("Failed to logout: %d %s", resp.status_code, resp.text)

        return False

    def _reset_token(self, timeout=10) -> Any:
        """ Reset authentication token in long running sessions """

        if self.auth is None:
            raise RuntimeError("User is not logged in")

        if self.auth.get('internal'):
            return

        exp_time = self.auth['token_time'] + datetime.timedelta(
            minutes=timeout)

        if exp_time < datetime.datetime.now():
            logging.warning("Reseting token due to timeout")

            auth = copy.deepcopy(self.auth)
            self.logout()

            return self.login(auth['username'], auth['password'])

        return False

    def shares(self,
               client_id: str = None,
               limit: int = 100,
               offset: int = None,
               fields: str = None) -> Any:

        params: Dict[str, Union[str, int]] = {}

        if client_id is not None:
            params.update({"client_id": client_id})

        if limit is not None:
            params.update({"limit": limit})

        if offset is not None:
            params.update({"offset": offset})

        if fields is not None:
            params.update({"fields": fields})

        self._reset_token()

        return depaginate(self.auth, Api.URL + 'shares', params=params)

    def mobileObservations(self,
                           _id: str = None,
                           share_id: str = None,
                           date_from_created: datetime.datetime = None,
                           date_to_created: datetime.datetime = None,
                           date_from_recorded: datetime.datetime = None,
                           date_to_recorded: datetime.datetime = None,
                           geometry_polygon: Polygon = None,
                           parameter_type: str = None,
                           client_id: str = None,
                           limit: int = 100,
                           offset: int = None,
                           fields: str = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = parseMobileObservationsQuery(
            share_id=share_id,
            date_from_created=date_from_created,
            date_to_created=date_to_created,
            date_from_recorded=date_from_recorded,
            date_to_recorded=date_to_recorded,
            geometry_polygon=geometry_polygon,
            parameter_type=parameter_type,
            client_id=client_id,
            limit=limit,
            offset=offset,
            fields=fields)

        self._reset_token()

        if _id is not None:
            return depaginate(
                self.auth,
                Api.URL + 'mobileObservations/' + _id,
                params=params)
        else:
            return self._mobileObservations(params=params)

    def _mobileObservations(self, params) -> List[Any]:
        return depaginate(
            self.auth, Api.URL + 'mobileObservations', params=params)

    def annotations(self,
                    _id: str = None,
                    share_id: str = None,
                    mobile_observation_id: str = None,
                    date_from_created: datetime.datetime = None,
                    date_to_created: datetime.datetime = None,
                    date_from_recorded: datetime.datetime = None,
                    date_to_recorded: datetime.datetime = None,
                    geometry_polygon: Polygon = None,
                    client_id: str = None,
                    limit: int = 100,
                    offset: int = None,
                    fields: str = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = {}

        if share_id is not None:
            params.update({"share_id": share_id})

        if mobile_observation_id is not None:
            params.update({"mobile_observation_id": mobile_observation_id})

        if date_from_created is not None:
            params.update({
                "date_from_created":
                datetimeToQueryParam(date_from_created)
            })

        if date_to_created is not None:
            params.update({
                "date_to_created":
                datetimeToQueryParam(date_to_created)
            })

        if date_from_recorded is not None:
            params.update({
                "date_from_recorded":
                datetimeToQueryParam(date_from_recorded)
            })

        if date_to_recorded is not None:
            params.update({
                "date_to_recorded":
                datetimeToQueryParam(date_to_recorded)
            })

        if geometry_polygon is not None:
            params.update({"geometry_polygon": polygonToCsv(geometry_polygon)})

        if client_id is not None:
            params.update({"client_id": client_id})

        if limit is not None:
            params.update({"limit": limit})

        if offset is not None:
            params.update({"foffset": offset})

        if fields is not None:
            params.update({"fields": fields})

        if _id is not None:
            self._reset_token()

            return depaginate(
                self.auth, Api.URL + 'annotations/' + _id, params=params)

        self._reset_token()

        return depaginate(self.auth, Api.URL + 'annotations', params=params)

    def notifications(self,
                      _id: str = None,
                      share_id: str = None,
                      client_id: str = None,
                      report_job_id: str = None,
                      date_from_created: datetime.datetime = None,
                      date_to_created: datetime.datetime = None,
                      date_from_updated: datetime.datetime = None,
                      date_to_updated: datetime.datetime = None,
                      get_content: bool = None,
                      all_users: bool = None,
                      limit: int = 100,
                      offset: int = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = {}

        if share_id is not None:
            params.update({"share_id": share_id})

        if client_id is not None:
            params.update({"client_id": client_id})

        if report_job_id is not None:
            params.update({"report_job_id": report_job_id})

        if date_from_created is not None:
            params.update({
                "date_from_created":
                datetimeToQueryParam(date_from_created)
            })

        if date_to_created is not None:
            params.update({
                "date_to_created":
                datetimeToQueryParam(date_to_created)
            })

        if date_from_updated is not None:
            params.update({
                "date_from_updated":
                datetimeToQueryParam(date_from_updated)
            })

        if date_to_updated is not None:
            params.update({
                "date_to_updated":
                datetimeToQueryParam(date_to_updated)
            })

        if get_content is not None:
            params.update({"get_content": str(get_content).lower()})

        if all_users is not None:
            params.update({"all_users": str(all_users).lower()})

        if limit is not None:
            params.update({"limit": limit})

        if offset is not None:
            params.update({"offset": offset})

        if _id is not None:
            self._reset_token()

            return depaginate(
                self.auth, Api.URL + 'notifications/' + _id, params=params)

        self._reset_token()

        return depaginate(self.auth, Api.URL + 'notifications', params=params)

    def notificationsOperationsRecipients(self, report_job_id: str):

        params: Dict[str, Union[str, int]] = {}

        if isinstance(report_job_id, str):
            params.update({"report_job_id": report_job_id})
        else:
            raise ValueError(
                "report_job_id should be type '{}', instead it is type: '{}'".
                format(type(''), type(report_job_id)))

        return api_get(
            self.auth,
            Api.URL + 'notifications/operations/recipients',
            params=params)

    def getNetwork(self,
                   query: Dict,
                   observation_parameter: List[str],
                   geometry_name: str,
                   geometry_polygon_area: Polygon = None) -> Any:

        geometry_polygon_area_str = polygonToCsv(geometry_polygon_area)
        query_params = parseMobileObservationsQuery(**query)

        data = {
            "mobile_observations_query": query_params,
            "observation_parameter": observation_parameter,
            "geometry_name": geometry_name,
            "geometry_polygon_area": geometry_polygon_area_str,
            "auth": {
                "auth_token": self.auth['authToken'],
                "user_id": self.auth['userId']
            }
        }

        self._reset_token()
        response = requests.post(
            Api.NETWORK_ANALYZER_URL + 'paint', json=data, timeout=240)

        if response.status_code is not 200:
            return {}

        return response.json()

    def create_notification(self,
                            report_job_id: str,
                            title: str,
                            content: str,
                            user_ids: List[str],
                            all_users: bool = None,
                            share_id: str = None,
                            client_id: str = None,
                            send_email: bool = None,
                            email_title: str = None,
                            email_content: str = None) -> Any:

        data: Dict[str, Union[str, int]] = {}

        if report_job_id is not None:
            data.update({"report_job_id": report_job_id})

        if share_id is not None:
            data.update({"share_id": share_id})

        if client_id is not None:
            data.update({"client_id": client_id})

        if user_ids is not None:
            data.update({"user_ids": ','.join(user_ids)})

        if all_users is not None:
            data.update({"all_users": str(all_users).lower()})

        if title is not None:
            data.update({"title": title})

        if content is not None:
            data.update({"content": content})

        if send_email is not None:
            data.update({"send_email": str(send_email).lower()})

        if email_title is not None:
            data.update({"email_title": email_title})

        if email_content is not None:
            data.update({"email_content": email_content})

        self._reset_token()

        return api_post(self.auth, Api.URL + 'notifications', data=data)

    def update_notification(self,
                            _id: str,
                            title: str = None,
                            content: str = None,
                            send_email: bool = None,
                            email_title: str = None,
                            email_content: str = None) -> Any:

        data: Dict[str, Union[str, int]] = {}

        if not isinstance(_id, str):
            raise ValueError(
                "_id should be type '{}', instead it is type: '{}'".format(
                    type(''), type(_id)))

        if title is not None:
            data.update({"title": title})

        if content is not None:
            data.update({"content": content})

        if send_email is not None:
            data.update({"send_email": str(send_email).lower()})

        if email_title is not None:
            data.update({"email_title": email_title})

        if email_content is not None:
            data.update({"email_content": email_content})

        if not data:
            raise ValueError(
                "you should provide at least one value for the update")

        self._reset_token()

        return api_patch(
            self.auth,
            '{base}notifications/{_id}'.format(base=Api.URL, _id=_id),
            data=data)

    def download_file(self, video_doc: Dict, filename: str) -> Any:
        if self._reset_token():
            self.downloader.set_headers(self.auth)

        url = os.path.dirname(video_doc['video_url_hd']) + "/" + filename

        return self.downloader.download_file(url)

    def frame_classifier(self, url, classifier='metsateho_bottleneck'):
        url = url + '&classifier=' + classifier

        self._reset_token()

        _, headers = _api_generic(
            requests.get,
            success_code=200,
            auth_data=self.auth,
            skip_output=True,
            url=url)

        if headers is None:
            return None

        pred = headers.get('x-prediction', None)

        if pred is not None:
            pred = json.loads(pred)

        return pred
