from geojson import Polygon
from datetime import datetime


def polygonToCsv(polygon: Polygon) -> str:
    if not polygon.is_valid:
        raise ValueError("Polygon is not valid: {}".format(polygon.errors()))

    if len(polygon.coordinates) != 1:
        raise ValueError("API supports only a single LinearRing in queries")

    return ','.join([str(a) for c in polygon.coordinates[0] for a in c])


def datetimeToQueryParam(time: datetime) -> str:
    return time.isoformat()
