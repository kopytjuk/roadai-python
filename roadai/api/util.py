import logging
import requests
from requests.exceptions import RequestException
from typing import Dict, Optional, Any, Tuple, List, Union
from urllib import parse


def depaginate(auth_data: Dict,
               url: str,
               params: Dict[str, Union[str, int]] = None) -> List[Any]:

    if params is None:
        params = {}

    if "offset" not in params:
        params["offset"] = 0

    if "limit" not in params:
        params["limit"] = 100

    entries: List[Any] = []

    while True:
        entries_page, links = api_get(auth_data, url, params=params)
        if entries_page is None:
            return entries

        cut_index = min(int(params["limit"]) - len(entries), len(entries_page))
        entries.extend(entries_page[:cut_index])

        next_offset = parse_next_offset(links)

        if next_offset is None:
            break
        else:
            params["offset"] = next_offset

    return entries


def api_get(auth_data: dict,
            url: str,
            params: Dict[str, Union[str, int]] = None) -> Tuple[Any, Any]:
    json_data, headers = _api_generic(
        requests.get,
        success_code=200,
        auth_data=auth_data,
        url=url,
        params=params)

    links = None
    if headers:
        links = extract_header_paging_links(headers)

    return (json_data, links)


def api_post(auth_data: dict,
             url: str,
             data: Dict[str, Union[str, int]] = None) -> Any:
    json_data, _ = _api_generic(
        requests.post,
        success_code=201,
        auth_data=auth_data,
        url=url,
        data=data)
    return json_data


def api_patch(auth_data: dict,
              url: str,
              data: Dict[str, Union[str, int]] = None) -> Any:
    json_data, _ = _api_generic(
        requests.patch,
        success_code=204,
        auth_data=auth_data,
        url=url,
        data=data)
    return json_data


def _api_generic(method,
                 success_code: int,
                 auth_data: dict,
                 url: str,
                 params: Dict[str, Union[str, int]] = None,
                 data: Dict[str, Union[str, int]] = None,
                 skip_output=False) -> Tuple[Any, Any]:
    """ Authenticated request """

    if not hasattr(method, '__call__'):
        raise ValueError("method should be callable")

    if params is not None and 'limit' not in params:
        params.update({"limit": 100})

    if auth_data.get('internal'):
        hdrs = {'Host': 'superuser', 'X-User-Id': auth_data['userId']}
    else:
        hdrs = {
            'X-Auth-Token': auth_data['authToken'],
            'X-User-Id': auth_data['userId']
        }

    try:
        rsp = method(url, params=params, data=data, headers=hdrs, timeout=30)
    except RequestException as err:
        logging.error("IOError: %s", str(err))
        return (None, None)

    if rsp.status_code == success_code:
        try:
            if skip_output:
                ret = None
            else:
                ret = '' if rsp.text == '' else rsp.json()

            return (ret, rsp.headers)
        except ValueError as err:
            logging.error("Failed to parse JSON: %s %s", rsp.text, str(err))
            return (None, rsp.headers)

    logging.error("Request: %s: %d, %s", rsp.url, rsp.status_code, rsp.text)
    return (None, None)


def extract_header_paging_links(headers) -> Optional[Dict]:
    """ Extract links from response headers """
    link_head = headers.get('Link', None)
    if not link_head:
        return None

    link_headers = link_head.replace(" ", "").split(",")
    links = {}
    for link in link_headers:
        key = link.split(";")[1].split("=")[1].replace('"', '')
        value = link.split(";")[0]
        links[key] = value
    return links


def parse_next_offset(links) -> Union[int, None]:
    if links is None or 'next' not in links or len(links['next']) < 3:
        return None

    url = links['next'][1:-1]
    o = parse.urlparse(url)
    qp = parse.parse_qs(o.query)

    if 'offset' not in qp:
        return None

    return int(qp['offset'][0])
