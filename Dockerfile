FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive

# Install apt dependencies
RUN apt-get update && apt-get install -y \
        locales python3-dev tzdata curl ca-certificates  \
    && rm -rf /var/lib/apt/lists/*

RUN update-alternatives --install "/usr/bin/python" "python" "/usr/bin/python3" 10
RUN cd /tmp && curl -fSsL -O https://bootstrap.pypa.io/get-pip.py \
    && python3 get-pip.py \
    && rm get-pip.py \
    && pip install --upgrade --no-cache-dir wheel setuptools Cython nose mypy yapf requests tuspy geojson

RUN pip3 --no-cache-dir install coverage


# Configure timezone
RUN echo "UTC" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata

# Configure locales
RUN sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
RUN /usr/sbin/locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8


# Move source files
COPY ./roadai /roadai/
COPY ./tests.sh /tests.sh
COPY ./setup.py /setup.py
COPY ./README.md /README.md
