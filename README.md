## roadai-python

[![pipeline status](https://gitlab.com/vionice/roadai-python/badges/master/pipeline.svg)](https://gitlab.com/vionice/roadai-python/commits/master)
[![coverage report](https://gitlab.com/vionice/roadai-python/badges/master/coverage.svg)](https://gitlab.com/vionice/roadai-python/commits/master)

A python library to access Vaisala RoadAI API: https://swagger.vionice.io  
Documentation: https://vionice.gitlab.io/roadai-python/

## Code example

Import and login
```python
from roadai.api import Api

api = Api()
api.login('john.doe@anonymous.com', 'myverysecretpassword')
```

List shares you have access to
```python
shares = api.shares()

for share in shares:
	print(share)
```

Download 10 most recent videos from a given share
```python
query = {
    "share_id": shares[0]["id"],
    "limit": 10
}

observations = api.mobileObservations(**query)

for obs in observations:
	print(obs)
```

By logging out, you explicitly invalidate the login token that was created for you
```python
api.logout()
```


## Installation
```
sudo pip install --upgrade git+https://gitlab.com/vionice/roadai-python
```
