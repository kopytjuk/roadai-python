.PHONY: build test-local test dev docs

.DEFAULT_GOAL := dev
IMAGE ?= roadai-python
CONTAINER ?= roadai-python-container

build:
	docker pull ubuntu:18.04
	docker build --tag $(IMAGE) .

dev: build
	docker rm --force $(CONTAINER); \
	docker run --name $(CONTAINER) --rm --interactive --tty \
		--volume ${PWD}/roadai:/roadai/:ro \
		--volume ${PWD}/test_data:/test_data \
		--workdir /roadai \
		--entrypoint /bin/bash $(IMAGE)


test-local:
	docker run --rm \
		--volume ${PWD}/roadai:/roadai/:ro \
		--volume ${PWD}/test_data:/test_data \
		--volume ${PWD}/tests.sh:/tests.sh:ro \
		$(IMAGE) ./tests.sh

test:
	docker build --file Dockerfile-setup --build-arg image=${IMAGE} .
	docker run --rm \
		$(IMAGE) ./tests.sh

docs:
	cd docs && $(MAKE) singlehtml
