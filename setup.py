import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="roadai_pkg",
    version="0.0.1",
    author="Petri Hienonen",
    author_email="author@vaisala.com",
    description="RoadAI python API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vionice/roadai-python.git",
    packages=setuptools.find_packages(),
    install_requires=["requests", "tuspy", "geojson"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
